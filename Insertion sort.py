
def insertion_sort(arr):
    if len(arr) > 1:
        master_key = 1
        while master_key < len(arr):
            if arr[master_key] < arr[master_key-1]:
                compare_key = master_key
                while compare_key-1 >= 0:
                    if arr[compare_key] < arr[compare_key-1]:
                        arr[compare_key], arr[compare_key-1] = arr[compare_key-1], arr[compare_key]
                        compare_key -= 1
                    else:
                        compare_key = -1
            master_key += 1


if __name__ == "__main__":
    arr = [89754567,10,9,8,4,2,3,4,5,5,999,1098]
    print(arr)
    insertion_sort(arr)
    print("Insertion Sort result:")
    print(arr)

