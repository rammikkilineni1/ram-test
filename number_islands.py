# Input: [[1,1,1,0],
#         [1,0,1,0],
#         [0,0,0,1],
#         [0,0,1,1]]
import sys

def get_num_islands(input):
    if input == None or len(input) == 0:
        return 0
    
    num_islands = 0
    for i in range(0, len(input)):
        for j in range(0, len(input[i])):
            num_islands += dfs(input, i, j)
    
    return num_islands

def dfs(input, i, j):
    if (i < 0 or i >= len(input) or j < 0 or j >= len(input[i]) or input[i][j] == 0):
        return 0
    
    input[i][j] = 0
    dfs(input, i+1, j)
    dfs(input, i-1, j)
    dfs(input, i, j+1)
    dfs(input, i, j-1)
    return 1

if __name__ == "__main__":
    print("Number of Islands: {}".format(get_num_islands([[1,1,1,0],[1,0,1,0],[0,1,1,1],[0,1,1,1]])))